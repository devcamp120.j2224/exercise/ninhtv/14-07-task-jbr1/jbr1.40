public class InvoiceItem {
    String id;
    String desc;
    int qty;
    double unitPrice;
    // hàm được gọi khi không truyền tham số 
    public InvoiceItem(){
        super();
    }
    // hàm được gọi khi truyền tham số
    public InvoiceItem(String id, String desc, int qty, double unitPrice) {
        this.id = id;
        this.desc = desc;
        this.qty = qty;
        this.unitPrice = unitPrice;
    }
    // getter setter
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public int getQty() {
        return qty;
    }
    public void setQty(int qty) {
        this.qty = qty;
    }
    public double getUnitPrice() {
        return unitPrice;
    }
    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }
    // hàm tính toán hóa đơn
    public double getTotal(){
        return this.unitPrice*this.qty;
    }
    // to string
    @Override
    public String toString() {
        return "InvoiceItem [desc=" + desc + ", id=" + id + ", qty=" + qty + ", unitPrice=" + unitPrice + "]";
    }
    
    
}
